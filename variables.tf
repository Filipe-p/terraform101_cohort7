# variables 
variable "identity_cohort_name" {
    description = "this is the cohort and name of the person"
    default = "cohort7-filipe2var"
}

variable "vpc_id" {
    default = "vpc-4bb64132"
}
