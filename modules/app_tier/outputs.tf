output "app_sg_id" {
    description = "this is the sg id of the app"
    value = aws_security_group.sg_webapp.id
}

output "ip_app" {
    description = "ip of the app"
    value = aws_instance.web.ip
}

# example