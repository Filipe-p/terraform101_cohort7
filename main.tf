# Provider
## define where to create resources and how to authenticate yourself.
## Options: Get key from vauls, from ENV or leave blank and assume AWS Role (needs to be set to machine)
provider "aws" {
    region = "eu-west-1"
    #  access_key = "my-access-key" This is where you could interpolate from ENV 
    #  secret_key = "my-secret-key" This is where you could interpolate from ENV 
}

# Resources
## Resources map to logical or physical things in a cloud provider 
## for example a ec2(physical virtual machine) instance of a SG(logical security group)
## each provider has it's library of resources. Use the documentation
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs

# how do we create a VPC? 

# How do we make IGW?
## associate with new vpc

# How do we make NACL?
## associate new subnet

# how do we make a route table? 

# how do we make a subnet? 
## provive new vpc id


# Call the app_tier modules
module "app_tier" {
  source = "./modules/app_tier"
  # Pass some variables
  vpc_id = var.vpc_id
  identity_cohort_name = var.identity_cohort_name

}

module "db_tier" {
    source = "./modules/db_tier"
    #passing some variable

    #passing variables that exist in app_tier
    #app_sg_id = module.app_tier.app_sg_id
    #app_id = module.app_tier.app_id
}