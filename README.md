# Terraform 101 :taco:

**Introduction**

Terraform is part of Infrastructure as Code, which is when you declerativly make infrastructre out of code. Your infrastructure is code, the rest is abstracted by our Cloud providers as they provide IaaS (Infrastructure as a Service).

In IAC, you have configuration management and orchestration tools. 

**Configuration management** 
Setting up individual servers to perfect desired state. 
Or managing their mutations. 

- Chef
- Ansible 
- Shell
- Puppet

**Orchestration Management tools**
These manages the networking and actual infrastructure (size of machines, subnet, vpc, NACL, SG, IGW, Routes). And deploy intances (machines) into these from AMIs (for example). They can also run small init script (User Data)


- Terraform
- Cloudformation (aws terraform)

## Terraform Objective

The objective of terraform **is to launch Build VPCs, Launch AMIs** (including running init scripts and start services)

**Example Terraform usage**
-  Create VPC
-  Setup SG and NCL and subnets
-  Deploys AMI's into respective subnets
-  Associate SG to instances
-  Runs init scripts


**Example usage in DevOps**
- Origin code exist in bitbucket/github
- Automation gets triggered from change in source
- CI pull code into Jenkins and runs tests
- Passing tests trigger next job of pipeline (CD)
- Jenkins uses Ansible to change machine to have new code
- Jenkins uses Packer to deviler an AMI 
- Jenkins uses Terraform to deploy AMI into production (CD)


## Installing Terraform

go to terraform and find out

## Givin permissions

If running from your machine (thus not inside AWS) - We need to give it some AWS secrete keys and access keys. 

Most client will manage their keys differently depending of the level of DevSecOps sophistication. 

### 🚓🚓🚨🚨🚨 DO NOT 🚨🚨🚨🚓🚓

- Hard code your keys with your code. 
- Ever
- Have them on a side file that is ignored by .gitignore.. Still really poor. 

### Possible ways of key management

- Set them as environment variables for machine & interpolate (low level of sophistication)
- AIM roles (if you are running in the cloud)
- Encrypted vaults locally (like ansible vault)
- Cloud Vaults (Hashicorp, Key Management Services (KMS from AWS))

Things you want - them to be encrypted. 

## example and code 

Example ec2 deployment: 

```terraform 


```

## Terraform Main Commands

Terraform main commands are: 
- terraform init
- terraform plan
- terraform apply
- terraform destroy

## Terrraform main sections 
- providers
- resources
  - aws_instance
- variables


### Resource

Syntax

```terraform 
resource "specific_resource " "name_in_tf" {
  param_resource = "value"
  other_param = "value"
}
```
- `specific_resource` is a resource withing the libabry of the providers
- `name_in_tf` is for calling this resource ins tf
- `param_resource` are the specific parameters that can be defined for that resource. For example a ec2 might requiere an AMI id where a vpc might need a IP range. you need to check your docs 

### how do we create a VPC? 

### How do we make IGW?
#### associate with new vpc

### How do we make NACL?
#### associate new subnet

### how do we make a route table? 

### how do we make a subnet? 
#### provive new vpc id


## Modules

how to call a module:
```terraform
module "app_tier" {
  source = "./modules/app_tier"

```

then you need to passing variables

```terraform
module "app_tier" {
  source = "./modules/app_tier"
  # Pass some variables
  vpc_id = var.vpc_id
  identity_cohort_name = var.identity_cohort_name

```

In your module you must create the follow file.

- main.tf
- variables.tf

In  the /modules/app_tier/variables.tf add:

```terraform
variable "identity_cohort_name" {
}

variable "vpc_id" {
}
```

these will be place holder to be used in your /modules/app_tier/main.tf


Finally if you want to output variables, specify them in the output: 
/modules/app_tier/ouputs.tf
```terrafomr
output "ip_app" {
    description = "ip of the app"
    value = aws_instance.web.ip
}
```

Always following the referencing system of `resource.resource_name.parameter`




